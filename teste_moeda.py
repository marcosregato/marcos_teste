# *-* coding: utf-8 *-*
__author__ = 'marcos'

'''
    Eu entendi que só tem 5 moedas com so valores, 1,5,7,9 e 11.
    não pode repiti as moedas, então fiz algumas combinções.
    O valor maior, que podemos chegar somando todas as moedas é 33
    e o menor valor é 6. Tem algumas combinações que somada sai o mesmo
    valor. Exemplo:
            1 + 5 = 6
            5 + 1 = 6

'''
print "Digite o valor."
numero = input()

combinacao_1 =  1+5  #6
combinacao_2 =  1+7  #8
combinacao_3 =  1+9  #10
combinacao_4 =  1+11 #12
combinacao_5 =  5+9  #14
combinacao_6 =  5+11 #16

#--

combinacao_7 = 7+9  #16
combinacao_8 = 7+11 #18
combinacao_9 = 9+1  #10
combinacao_10 = 9+5  #14
combinacao_11 = 9+11 #21
combinacao_12 = 11+1 #12
combinacao_13 = 11+5 #16

#---

combinacao_14 = 1+5+7  #13 <<<<<<<<<<
combinacao_15 = 5+7+9  #21 <<<<<<<
combinacao_16 = 9+11+1 #22 <<<<<<<<
combinacao_17 = 7+9+11 #27 <<<<<<<<<
combinacao_18 = 1+7+9  #17 <<<<<<<
combinacao_19 = 5+9+11 #25 <<<<<<<
combinacao_20 = 7+11+1 #19
combinacao_21 = 9+1+5  #15
combinacao_22 = 11+5+7 #23

#---

combinacao_23 = 5+7+9+11 #32
combinacao_24 = 7+9+11+1 #28
combinacao_25 = 9+11+1+5 #26
combinacao_26 = 11+1+5+7 #24
combinacao_27 = 7+11+5+7 #30
combinacao_28 = 11+5+7+9 #32
combinacao_29 = 1+5+7+9+11 #33

if combinacao_1 == numero:
    print "1 + 1 = 2\n"

elif combinacao_2 == numero:
    print "1 + 7 = 8\n"
    print "combinações que sai o mesmo resultado"
    print "7 + 1 = 8"

elif combinacao_3 == numero:
    print "1 + 9 = 10\n"
    print "combinações que sai o mesmo resultado"
    print "9 + 1 = 10"

elif combinacao_4 == numero:
    print "1 + 11 = 12\n"
    print "combinações que sai o mesmo resultado"
    print "11 + 1 = 12"

elif combinacao_5 == numero:
    print "5 + 9  \n"
    print "combinações que sai o mesmo resultado"
    print "9 + 5"

elif combinacao_6 == numero:
    print "5 + 11 \n"
    print "combinações que sai o mesmo resultado"
    print "11 + 5"

elif combinacao_7 == numero:
    print "7 + 9 \n"
    print "combinações que sai o mesmo resultado"
    print "9 + 7"

elif combinacao_8 == numero:
    print "7 + 11 \n"
    print "combinações que sai o mesmo resultado"
    print "11 + 7"

elif combinacao_9 == numero:
    print "9 + 1 \n"
    print "combinações que sai o mesmo resultado"
    print "9 + 1"

elif combinacao_10 == numero:
    print "9 + 5 \n"
    print "combinações que sai o mesmo resultado"
    print "5 + 9"

elif combinacao_11 == numero:
    print "9 + 11 \n"
    print "combinações que sai o mesmo resultado"
    print "11 + 9"

elif combinacao_12 == numero:
    print "11 + 1 \n"
    print "combinações que sai o mesmo resultado"
    print "1 + 11"

elif combinacao_13 == numero:
    print "11 + 5 \n"
    print "combinações que sai o mesmo resultado"
    print "5 + 11"

elif combinacao_14 == numero:
    print "1+5+7 \n"
    print "combinações que sai o mesmo resultado"
    print "7 + 5 + 1 = 13"
    print "7 + 1 + 5 = 13"
    print "5 + 7 + 1 = 13"
    print "5 + 1 + 7 = 13"

elif combinacao_15 == numero:
    print "5 + 7 + 9 \n"
    print "combinações que sai o mesmo resultado"
    print "7 + 5 + 9 = 21"
    print "9 + 7 + 5 = 21"
    print "9 + 5 + 7 = 21"
    print "7 + 9 + 5 = 21"
    print "5 + 9 + 7 = 21"


elif combinacao_16 == numero:
    print "9 + 11 + 1 \n"
    print "combinações que sai o mesmo resultado"
    print "9 + 1 + 11 = 21"
    print "1 + 9 + 11 = 21"
    print "1 + 11 + 9 = 21"
    print "11 + 1 + 9 = 21"
    print "11 + 9 + 1 = 21"

elif combinacao_17 == numero:
    print "7 + 9 + 11 =27\n"
    print "combinações que sai o mesmo resultado"
    print "7 + 11 + 9 = 27"
    print "11 + 9 + 7 = 27"
    print "11 + 7 + 9 = 27"
    print "9 + 11 + 7 = 27"
    print "9 + 7 + 11 = 27"

elif combinacao_18 == numero:
    print "1 + 7 + 9 = 17\n"
    print "combinações que sai o mesmo resultado"
    print "1 + 9 + 7 = 17"
    print "9 + 1 + 7 = 17"
    print "9 + 7 + 1 = 17"
    print "7 + 9 + 1 = 17"
    print "7 + 1 + 9 = 17"

elif combinacao_19 == numero:
    print "5 + 9 + 11 = 25\n"
    print "combinações que sai o mesmo resultado"
    print "5  + 11 + 9  = 25"
    print "11 + 5  + 9  = 25"
    print "11 + 9  + 5  = 25"
    print "9  + 11 + 5  = 25"
    print "9  + 5  + 11 = 25"

elif combinacao_20 == numero:
    print "7 + 11 + 1 = 19 \n"
    print "combinações que sai o mesmo resultado"
    print "7  + 1 + 11  = 19"
    print "11 + 11 + 1  = 19"
    print "11 + 1  + 7  = 19"
    print "1  + 11 + 7  = 19"
    print "1  + 7  + 11 = 19"

elif combinacao_21 == numero:
    print "9 + 1 + 5 = 15 \n"
    print "combinações que sai o mesmo resultado"
    print "9 + 5 + 1 = 15"
    print "5 + 9 + 5 = 15"
    print "5 + 1 + 9 = 15"
    print "1 + 9 + 5 = 15"
    print "1 + 5 + 9 = 15"


elif combinacao_22 == numero:
    print "11 + 5 + 7 = 23\n"
    print "combinações que sai o mesmo resultado"
    print "11 + 7 + 5 = 23"
    print "5 + 11 + 7 = 23"
    print "5 + 7 + 11 = 23"
    print "7 + 5 + 11 = 23"
    print "7 + 11 + 5 = 23"

elif combinacao_23 == numero:
    print "5 + 7 + 9 + 11 = 32\n"
    print "combinações que sai o mesmo resultado"
    print "5 + 9  + 7  + 11 = 32"
    print "5 + 7  + 11 + 9  = 32"
    print "7 + 5  + 9  + 11 = 32"
    print "7 + 5  + 11 + 9  = 32"
    print "7 + 9  + 5  + 11 = 32"
    print "9 + 7  + 5  + 11 = 32"
    print "9 + 11 + 5 + 7   = 32"

elif combinacao_24 == numero:
    print "7 + 9 + 11 + 1 = 28\n"
    print "combinações que sai o mesmo resultado"
    print "7  + 9 + 1  + 11 = 28"
    print "11 + 7 + 9  + 1  = 28"
    print "11 + 1 + 9  + 11 = 28"
    print "9  + 7 + 11 + 1  = 28"
    print "9  + 7 + 1  + 11 = 28"

elif combinacao_25 == numero:
    print "9 + 11 + 1 + 5 = 26\n"
    print "combinações que sai o mesmo resultado"
    print "9 + 11 + 5 + 1 = 26"
    print "11 + 1 + 9 + 5 = 26"
    print "11 + 1 + 5 + 9 = 26"
    print "5 + 1 + 11 + 9 = 26"
    print "5 + 11 + 1 + 9 = 26"

elif combinacao_26 == numero:
    print "11 + 1 + 5 + 7 = 24\n"
    print "combinações que sai o mesmo resultado"
    print "11 + 5 + 1 + 7 = 24"
    print "1 + 11 + 5 + 7 = 24"
    print "1 + 11 + 7 + 5 = 24"
    print "11 + 1 + 5 + 7 = 24"
    print "11 + 1 + 5 + 7 = 24"

elif combinacao_27 == numero:
    print "7 + 11 + 5 + 7 = 30\n"
    print "combinações que sai o mesmo resultado"
    print "7 + 11 + 7  + 5   = 30"
    print "5 + 7  + 11 + 7   = 30"
    print "5 + 7  + 7  + 11  = 30"
    print "7 + 5  + 11 + 7   = 30"
    print "7 + 5  + 7  + 11  = 30"

elif combinacao_28 == numero:
    print "11 + 5 + 7 + 9 = 32\n"
    print "combinações que sai o mesmo resultado"
    print "11 + 5 + 9 + 7 = 32"
    print "5 + 11 + 7 + 9 = 32"
    print "5 + 11 + 9 + 7 = 32"
    print "11 + 5 + 7 + 9 = 32"
    print "11 + 5 + 9 + 7 = 32"

elif combinacao_29 == numero:
    print "11 + 5 + 7 + 9 + 1 = 33\n"
    print "combinações que sai o mesmo resultado"
    print "11 + 5 + 9 + 7 + 1 = 33"
    print "5 + 11 + 7 + 9 + 1 = 33"
    print "5 + 11 + 9 + 7 + 1 = 33"
    print "11 + 5 + 7 + 9 + 1 = 33"
    print "11 + 5 + 9 + 7 + 1 = 33"

else:
    print "Nao tem combinação que sai esse resultado. Tente outro número"
#print moeda(input("Digite um numero :\n"))